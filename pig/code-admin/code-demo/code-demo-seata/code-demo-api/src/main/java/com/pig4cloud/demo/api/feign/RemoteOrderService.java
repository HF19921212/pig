package com.pig4cloud.demo.api.feign;

import com.pig4cloud.demo.api.entity.Order;
import com.pig4cloud.demo.api.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * name: 指定要调用的微服务的名字，用于服务发现，必填
 * value: 同name属性，alias for name
 * url: url一般用于调试，可以手动指定调用的绝对地址
 * configuration: Feign配置类，可以自定义Feign的Encoder、Decoder、LogLevel、Contract
 * decode404:当发生http 404错误时，如果该字段位true，会调用decoder进行解码，否则抛出FeignException，默认为false
 * fallback: 定义容错的处理类，当调用远程接口失败或超时时，会调用对应接口的容错逻辑，fallback指定的类必须实现@FeignClient标记的接口
 * fallbackFactory: 工厂类，用于生成fallback类示例，通过这个属性我们可以实现每个接口通用的容错逻辑，减少重复的代码
 * path: 定义当前FeignClient的统一前缀，设置context-path的服务，这个值如果不注意配置就404了
 */
@FeignClient(contextId= "remoteOrderService",
		     value = "code-demo-order"
)
public interface RemoteOrderService {

	String API_PREFIX = "/order";

	/**
     * 保存订单
	 * @param order
     * @return
     */
	@PostMapping( value = API_PREFIX + "/save" , consumes = "application/json")
	@ResponseBody
	R<Boolean> save(@RequestBody Order order);
}
