package com.pig4cloud.demo.order.client;

import com.pig4cloud.demo.api.entity.Order;
import com.pig4cloud.demo.api.feign.RemoteOrderService;
import com.pig4cloud.demo.api.util.R;
import com.pig4cloud.demo.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
public class RemoteOrderClient implements RemoteOrderService {

	private final OrderService orderService;

	@Override
	@PostMapping(API_PREFIX + "/save")
	public R<Boolean> save(@RequestBody Order order) {
		order.setChannel("渠道名称");
		order.setOrderDesc("订单描述");
		order.setPayTime(LocalDateTime.now());
		order.setOrderType(1);
		order.setAppSource("应用来源");
		order.setCreateTime(LocalDateTime.now());
		return R.ok(orderService.save(order));
	}

}
