package com.pig4cloud.demo.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.demo.api.entity.Order;
import com.pig4cloud.demo.order.mapper.OrderMapper;
import com.pig4cloud.demo.order.service.OrderService;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
}
