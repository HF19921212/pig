package com.pig4cloud.pig.upms;

import com.pig4cloud.pig.security.annotation.EnableCodeResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author pig archetype
 * <p>
 * 项目启动类
 */
@EnableFeignClients(basePackages = "com.pig4cloud.pig")
@EnableCodeResourceServer
/** 开启服务注册发现功能 **/
@EnableDiscoveryClient
@SpringBootApplication
public class CodeUpmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeUpmsApplication.class, args);
    }

}
