package com.pig4cloud.demo.server.controller;

import com.pig4cloud.demo.api.entity.Order;
import com.pig4cloud.demo.api.entity.User;
import com.pig4cloud.demo.api.feign.RemoteOrderService;
import com.pig4cloud.demo.api.feign.RemoteUserService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ServerController {

	private final RemoteUserService remoteUserService;

	private final RemoteOrderService remoteOrderService;

	/**
	 * AT 模式
	 * @return
	 */
	@GlobalTransactional(rollbackFor = Exception.class, timeoutMills = 60000,name = "domo-tx")
	@GetMapping("/at/test")
	public String test(){
		Order order = new Order();
		remoteOrderService.save(order);
		// by zero
		int i = 1 / 0;
		User user = new User();
		user.setId(24);
		remoteUserService.updateByVipEndTimeForUserId(user);

		return "success";
	}

}
