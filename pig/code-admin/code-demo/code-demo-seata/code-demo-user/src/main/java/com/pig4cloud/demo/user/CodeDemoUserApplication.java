package com.pig4cloud.demo.user;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class CodeDemoUserApplication {

	public static void main(String[] args) {
		//加密验签伪代码
		String MY_KEY = "a9e8fh34wf^&(%@&@HJK28JKFHr23e423!@34RNwe";
		String sign= "6cf27e2c47";
		String data = "123456789" + MY_KEY;
		String md5str = DigestUtil.md5Hex(data);
		// 索引10位置取10位长度
		String assertSign = md5str.substring(10, 20);
		if(StrUtil.isEmpty(assertSign) || !assertSign.equals(sign)){
			throw new RuntimeException("请求签名有误");
		}

		SpringApplication.run(CodeDemoUserApplication.class, args);
	}

}
