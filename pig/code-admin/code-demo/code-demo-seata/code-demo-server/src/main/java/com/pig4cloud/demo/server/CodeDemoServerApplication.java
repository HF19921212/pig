package com.pig4cloud.demo.server;

import com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

/** 在主程序位置加上注解 @EnableFeignClients ,确保能扫描到上面定义的 FeignClient 接口 **/
@EnableFeignClients(basePackages = "com.pig4cloud.demo.api.feign")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, IdentifierGeneratorAutoConfiguration.class})
public class CodeDemoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeDemoServerApplication.class, args);
	}

}
