package com.pig4cloud.demo.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.demo.api.entity.User;
import com.pig4cloud.demo.user.mapper.UserMapper;
import com.pig4cloud.demo.user.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
