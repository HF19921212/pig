package com.pig4cloud.demo.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
public class SecurityConfigUser extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private DataSource dataSource;

	/**
	 * 自动登录数据库操作
	 * @return
	 */
	@Bean
	public PersistentTokenRepository persistentTokenRepository(){
		JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
		jdbcTokenRepository.setDataSource(dataSource);
		//自动生成表
		//jdbcTokenRepository.setCreateTableOnStartup(true);
		return jdbcTokenRepository;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	/**
	 * 安全配置
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//注销
		http.logout().logoutUrl("/logout").logoutSuccessUrl("/").permitAll();
		//配置没有权限访问的自定义页面
		http.exceptionHandling().accessDeniedPage("/unauth.html");
		http.formLogin()						//自定义编写的登录页面
			.loginPage("/login.html")			//登录页面配置
			.loginProcessingUrl("/user/login")	//登录访问的路径,名称可以随便写,具体由springSecurity帮我们去执行
			.defaultSuccessUrl("/success.html")	//登录成功跳转路径
			.permitAll()						//允许操作
			.and().authorizeRequests()			//定义哪些url可以访问，哪些url不能访问,需要认证
			.antMatchers("/","/user/login").permitAll()	//设置登录不需要认证

			.anyRequest().authenticated()		//所有请求都可以访问

			.and().rememberMe().tokenRepository(persistentTokenRepository())
				.tokenValiditySeconds(60)	//设置有效时长，单位秒
				.userDetailsService(userDetailsService)

			.and().csrf().disable();			//关闭csrf防护
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

}
