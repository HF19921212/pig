package com.pig4cloud.demo.user.client;


import com.pig4cloud.demo.api.entity.User;
import com.pig4cloud.demo.api.feign.RemoteUserService;
import com.pig4cloud.demo.api.util.R;
import com.pig4cloud.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
public class RemoteUserClient implements RemoteUserService {

	private final UserService userService;

	@Override
	@PostMapping(API_PREFIX + "/updateByVipEndTimeForUserId")
	public R<Boolean> updateByVipEndTimeForUserId(@RequestBody User user) {
		user.setVipEndTime(LocalDateTime.now());
		return R.ok(userService.updateById(user));
	}

}
