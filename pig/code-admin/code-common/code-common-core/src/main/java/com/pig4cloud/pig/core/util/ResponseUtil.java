package com.pig4cloud.pig.core.util;

import lombok.SneakyThrows;

import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

/**
 * 数据返回
 *
 * @author he.fan
 */
public class ResponseUtil {

	@SneakyThrows
	public static void write(HttpServletResponse response, Object o){
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println(o.toString());
		out.flush();
		out.close();
	}

}
