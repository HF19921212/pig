package com.pig4cloud.pig.upms.service.impl;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pig.core.constant.constant.CommonConstants;
import com.pig4cloud.pig.core.constant.constant.SecurityConstants;
import com.pig4cloud.pig.security.service.CodeUser;
import com.pig4cloud.pig.upms.api.dto.UserInfo;
import com.pig4cloud.pig.upms.api.entity.SysUser;
import com.pig4cloud.pig.upms.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 自定义UserDetailsService 实现类
 *
 * @author he.fan
 * @变更历史 <table border="1" class="x"><thead><tr><th>时间</th><th>人员</th><th>内容</th></thead><tbody>
 * <tr><td>2021/8/29 20:41</td><td>何帆</td><td>新增</td></tr>
 * </tbody></table>
 */
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

	private final SysUserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//查询用户信息
		SysUser user = userService.getOne(Wrappers.<SysUser>query().lambda().eq(SysUser::getUsername, username));
		UserInfo userInfo = userService.getUserInfo(user);
		UserDetails userDetails = getUserDetails(userInfo);
		return userDetails;
	}

	/**
	 * 构建userdetails
	 * @param info 用户信息
	 * @return
	 */
	private UserDetails getUserDetails(UserInfo info) {
		if (info == null) {
			throw new UsernameNotFoundException("用户不存在");
		}

		Set<String> dbAuthsSet = new HashSet<>();
		if (ArrayUtil.isNotEmpty(info.getRoles())) {
			// 获取角色
			Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(SecurityConstants.ROLE + role));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

		}
		Collection<? extends GrantedAuthority> authorities = AuthorityUtils
				.createAuthorityList(dbAuthsSet.toArray(new String[0]));
		SysUser user = info.getSysUser();

		// 构造security用户
		return new CodeUser(user.getUserId(), user.getDeptId(), user.getUsername(),
				user.getPassword(),
				StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL), true, true, true, authorities);
	}

	public static void main(String[] args) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String password = bCryptPasswordEncoder.encode("123456");
		System.out.println(password);
	}

}
