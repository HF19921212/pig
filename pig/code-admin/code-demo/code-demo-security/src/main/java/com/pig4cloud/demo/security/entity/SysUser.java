package com.pig4cloud.demo.security.entity;

import lombok.Data;

@Data
public class SysUser {

	private String username;

	private String password;
}
