package com.pig4cloud.demo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.demo.api.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表
 *
 * @author pig code generator
 * @date 2021-06-18 12:29:00
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
