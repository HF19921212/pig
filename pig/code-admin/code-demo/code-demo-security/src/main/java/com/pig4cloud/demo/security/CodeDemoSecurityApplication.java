package com.pig4cloud.demo.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@MapperScan("com.pig4cloud.demo.security.mapper")
//开启注解
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true)
public class CodeDemoSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeDemoSecurityApplication.class, args);
	}

}
