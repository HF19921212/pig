package com.pig4cloud.demo.security.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestEndpoints {

	@GetMapping("/order/{id}")
	public String getOrder(@PathVariable String id) {
		//for debug
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return "order id : " + id;
	}

	@GetMapping("/test")
	public String test() {
		return "hello test";
	}

}