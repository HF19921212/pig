package com.pig4cloud.demo.db.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * 数据源代理
 */
@Configuration
public class DataSourceConfiguration {

	@Bean
	@ConfigurationProperties("spring.datasource")
	public DataSource hikariDataSource(){
		return new HikariDataSource();
	}

	/**
	 * 设置数据源代理
	 * @param hikariDataSource
	 * @return
	 */
	@Primary	//设置首选数据源
	@Bean("dataSource")
	public DataSourceProxy dataSourceProxy(DataSource hikariDataSource){
		return  new DataSourceProxy(hikariDataSource);
	}

}
