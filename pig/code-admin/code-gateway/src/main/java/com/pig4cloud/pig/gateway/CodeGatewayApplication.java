package com.pig4cloud.pig.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient	//在Nacons进行注册
public class CodeGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeGatewayApplication.class, args);
	}

}
