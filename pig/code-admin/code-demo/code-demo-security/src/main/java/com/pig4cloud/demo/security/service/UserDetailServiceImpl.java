package com.pig4cloud.demo.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pig4cloud.demo.security.entity.SysUser;
import com.pig4cloud.demo.security.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private SysUserMapper sysUserMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
		wrapper.eq("username",username);
		SysUser sysUser = sysUserMapper.selectOne(wrapper);
		if(sysUser == null){
			throw new UsernameNotFoundException("用户名不存在！");
		}
		//设置成超级管理员,生产环境从数据库获取
		List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admin");
		return new User(sysUser.getUsername(),new BCryptPasswordEncoder().encode(sysUser.getPassword()),auths);
	}

}
