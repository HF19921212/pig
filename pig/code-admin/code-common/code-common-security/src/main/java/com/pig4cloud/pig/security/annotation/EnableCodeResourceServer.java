package com.pig4cloud.pig.security.annotation;

import com.pig4cloud.pig.security.component.CodeResourceServerAutoConfiguration;
import com.pig4cloud.pig.security.component.CodeSecurityBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.annotation.*;

/**
 * @author hefan
 * @date 2021/10/28
 * <p>
 * 资源服务注解
 */
@Documented
@Inherited
@EnableResourceServer
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import({ CodeResourceServerAutoConfiguration.class,CodeSecurityBeanDefinitionRegistrar.class })
public @interface EnableCodeResourceServer {

}
