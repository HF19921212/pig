package com.pig4cloud.demo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.demo.api.entity.User;

public interface UserService extends IService<User> {

}
