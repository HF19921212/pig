package com.pig4cloud.demo.redistemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeDemoRedistemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeDemoRedistemplateApplication.class, args);
	}

}
