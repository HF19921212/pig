package com.pig4cloud.pig.auth.bcrypt;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Bcrypt就是一款加密工具，可以比较方便地实现数据的加密工作。你也可以简单理解为它内部自己实现了随机加盐处理
 * 例如，我们使用MD5加密，每次加密后的密文其实都是一样的，这样就方便了MD5通过大数据的方式进行破解。
 * Bcrypt生成的密文是60位的。而MD5的是32位的。
 * 使用BCrypt 主要是能实现每次加密的值都是不一样的。
 */
public class BcryptTest {

	public static void main(String[] args) {
		//用户密码
		String password = "123456";
		//密码加密
		BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
		//加密
		String newPassword = passwordEncoder.encode(password);
		System.out.println("加密密码为："+newPassword);
		//对比这两个密码是否是同一个密码
		boolean matches = passwordEncoder.matches(password, newPassword);
		System.out.println("两个密码一致:"+matches);
	}
}


