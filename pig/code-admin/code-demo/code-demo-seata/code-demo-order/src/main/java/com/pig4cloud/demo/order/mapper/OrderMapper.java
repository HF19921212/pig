package com.pig4cloud.demo.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig4cloud.demo.api.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author hefan
 * @since 2021/6/16
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {


}
