package com.pig4cloud.demo.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.demo.api.entity.Order;

public interface OrderService extends IService<Order>  {

}
