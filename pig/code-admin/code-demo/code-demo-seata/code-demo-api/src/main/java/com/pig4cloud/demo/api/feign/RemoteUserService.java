package com.pig4cloud.demo.api.feign;

import com.pig4cloud.demo.api.entity.User;
import com.pig4cloud.demo.api.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(contextId = "remoteUserService",
			 value = "code-demo-user"
)
public interface RemoteUserService {

	String API_PREFIX = "/user";

	/**
     * 开通会员
	 * @param user
     * @return
     */
	@PostMapping( value = API_PREFIX + "/updateByVipEndTimeForUserId" , consumes = "application/json")
	@ResponseBody
	R<Boolean> updateByVipEndTimeForUserId(@RequestBody User user);

}
