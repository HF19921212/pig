package com.pig4cloud.demo.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class CodeDemoOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeDemoOrderApplication.class, args);
	}

}
