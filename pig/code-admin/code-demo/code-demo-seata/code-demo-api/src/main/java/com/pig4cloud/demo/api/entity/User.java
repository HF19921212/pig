package com.pig4cloud.demo.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户表
 *
 * @author pig code generator
 * @date 2021-06-18 12:29:00
 */
@Data
@TableName("demo_user")
public class User implements Serializable {

	private static final long serialVersionUID = -3653862588168056946L;

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 头像
	 */
	private String headPicture;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * token
	 */
	private String token;
	/**
	 * 设备号
	 */
	private String deviceId;

	/**
	 * 1,微信;2,QQ;3,支付宝;4,微博
	 */
	private Integer loginType;

	/**
	 * 微信openid
	 */
	private String wxOpenid;
	/**
	 * 微信unionid
	 */
	private String wxUnionid;

	/**
	 * 激光id
	 */
	private String jiguangId;


	/**
	 * 支付宝账号alipay_account
	 */
	private String alipayAccount;

	/**
	 * 支付宝姓名 alipay_realname
	 */
	private String alipayRealname;


	/**
	 * 苹果用户标识符
	 */
	private String appleUserIdentifier;

	/**
	 * 金币数量
	 */
	private Long coin;
	/**
	 * 现金余额(分)
	 */
	private Long rmb;
	/**
	 * vip截止时间
	 */
	private LocalDateTime vipEndTime;
	/**
	 * 用户活跃天数(累计)
	 */
	private Integer activeDayCnt;
	/**
	 * 渠道
	 */
	private String channel;

	/**
	 * 系统 1-iOS 2-Android
	 */
	private Integer os;
	/**
	 * 创建时间
	 */
	private LocalDateTime createTime;
	/**
	 * 更新时间
	 */
	private LocalDateTime updateTime;
	/**
	 * 是否删除：0-未删除1-删除
	 */
	private Integer deleted;

	/**
	 * 登录时间
	 */
	private LocalDateTime logonTime;

}
