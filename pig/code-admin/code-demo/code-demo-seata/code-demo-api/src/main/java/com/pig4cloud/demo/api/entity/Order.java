package com.pig4cloud.demo.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import java.time.LocalDateTime;

/**
 * 支付订单表
 *
 * @author pig code generator
 * @date 2021-06-18 12:29:00
 */
@Data
@TableName("demo_pay_order")
public class Order {

	private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
	@TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * app来源
     */
    private String appSource;
    /**
     * 订单号
     */
    private String orderId;
    /**
     * 用户uid
     */
    private Integer uid;
    /**
     * 设备id
     */
    private String deviceId;
    /**
     * 渠道名称
     */
    private String channel;
    /**
     * 金额(分)
     */
    private Integer amount;
    /**
     * 第三方充值订单号
     */
    private String inpourNo;
    /**
     * 充值平台 1-h5 2-公众号 3-微信浏览器 4-PC扫码 5-手机QQ 6-APP 7-小程序
     */
    private Integer payOs;
    /**
     * 支付方式 1-微信 2-支付宝
     */
    private Integer payType;
    /**
     * 0-等待支付 1-支付成功
     */
    private Integer status;
    /**
     * 订单描述
     */
    private String orderDesc;
    /**
     * 商品id
     */
    private Integer productId;
    /**
     * 支付时间
     */
    private LocalDateTime payTime;
    /**
     * 订单类型 1-会员
     */
    private Integer orderType;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 是否删除：0未删除1删除
     */
    private Integer deleted;
}
