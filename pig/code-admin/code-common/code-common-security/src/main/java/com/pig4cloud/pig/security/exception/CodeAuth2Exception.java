package com.pig4cloud.pig.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pig4cloud.pig.security.component.CodeAuth2ExceptionSerializer;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @author hefan
 * @date 2021/11/21 自定义OAuth2Exception
 */
@JsonSerialize(using = CodeAuth2ExceptionSerializer.class)
public class CodeAuth2Exception extends OAuth2Exception {

	@Getter
	private String errorCode;

	public CodeAuth2Exception(String msg) {
		super(msg);
	}

	public CodeAuth2Exception(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;
	}

}
