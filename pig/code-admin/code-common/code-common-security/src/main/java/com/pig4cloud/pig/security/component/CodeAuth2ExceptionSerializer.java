package com.pig4cloud.pig.security.component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.pig4cloud.pig.core.constant.constant.CommonConstants;
import com.pig4cloud.pig.security.exception.CodeAuth2Exception;
import lombok.SneakyThrows;

/**
 * @author lengleng
 * @date 2021/11/21
 * <p>
 * OAuth2 异常格式化
 */
public class CodeAuth2ExceptionSerializer extends StdSerializer<CodeAuth2Exception> {

	public CodeAuth2ExceptionSerializer() {
		super(CodeAuth2Exception.class);
	}

	@Override
	@SneakyThrows
	public void serialize(CodeAuth2Exception value, JsonGenerator gen, SerializerProvider provider) {
		gen.writeStartObject();
		gen.writeObjectField("code", CommonConstants.FAIL);
		gen.writeStringField("msg", value.getMessage());
		gen.writeStringField("data", value.getErrorCode());
		// 资源服务器会读取这个字段
		gen.writeStringField("error", value.getMessage());
		gen.writeEndObject();
	}

}
