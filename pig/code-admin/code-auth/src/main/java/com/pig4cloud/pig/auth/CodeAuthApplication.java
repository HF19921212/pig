package com.pig4cloud.pig.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hefan
 * @date 2021年10月25日 认证授权中心
 */
@EnableFeignClients(basePackages = "com.pig4cloud.pig")
/** 开启服务注册和发现功能**/
@EnableDiscoveryClient
@SpringBootApplication
public class CodeAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeAuthApplication.class, args);
	}

}
